# sigarillo-serverless

> Look ma', no servers!

This project provides a set of AWS Lambda functions (backed by DynamoDB) for:

1. registering and verifying a [Signal](http://signal.org/) account
2. catching webhooks from other services, and formatting them into messages sent to Signal contacts or groups 

**Do not use for sensitive/private information.**

Supported use cases:

* dispatching [alertmanager](https://github.com/prometheus/alertmanager) alerts
* dispatching [datadog](https://datadoghq.com) alerts

## Setup

#### Prerequisites

* node 8.10
* [serverless](https://github.com/serverless/serverless)

OR

* Vagrant

### Get going using Vagrant

Assuming you have vagrant installed and know how to use
[aws-vault](https://github.com/99designs/aws-vault) or configure aws cli, this is the quickest way
to get up and running in a environment for working with this app.

```
vagrant up
vagrant ssh
cd signal-alertmanager-serverless
aws-vault add <YOUR_PROFILE_NAME>
```

## Configure

The service expects to find these parameters in the SSM Parameter store as `SecureString`s

* `/sigarillo-serverless/<STAGE>/SIGNAL_SHARED_SECRET`
* `/sigarillo-serverless/<STAGE>/SIGNAL_ALERTMANAGER_RECIPIENTS`

Where `<STAGE>` is the `development`/`production` stage you specify.

You can use the AWS cli to add them, or terraform, as you like.

As part of the serverless deployment, the functions will be given an IAM policy
that allows them read access to these params.

## Deploy

#### Deploy development

```
aws-vault exec <YOUR_PROFILE_NAME> -- serverless deploy -v --stage development
```

#### Deploy production

```
aws-vault exec <YOUR_PROFILE_NAME> -- serverless deploy -v --stage production
```

### Remove infrastructure

```
aws-vault exec <YOUR_PROFILE_NAME> -- serverless remove --stage XXX
```

## Usage Example with alertmanager

### 1. Register number

First, register a number with Signal. Do not use an existing number, unless
you're willing to lose all messages and keys. This will create a new signal
account for this number.

#### SMS Verification

To request registration with SMS verification:

```bash
curl -s  -XPOST -d '{"number": "+15555555555", verificationType: "SMS" }' https://<LAMBDA_ENDPOINT>/register?secret=<SERVER_SECRET>
```

#### Voice Verification

To request registration with voice verification:

```bash
curl -s  -XPOST -d '{"number": "+15555555555", verificationType: "VOICE" }' https://<LAMBDA_ENDPOINT>/register?secret=<SERVER_SECRET>
```

### 2. Verify number

Once you've received the verification code, verify it using:

```bash
curl -s  -XPOST -d '{"number": "+15555555555", code": "123456" }' https://<LAMBDA_ENDPOINT>/verify?secret=<SERVER_SECRET>
```

### 3. Send test alert

```bash
curl -s  -XPOST -d '{"number": "+15555555555", code": "123456" }' https://<LAMBDA_ENDPOINT>/alert?secret=<SERVER_SECRET>
```

### 4. Configure alertmanager

In your `alertmanager.yml` add this block to your `receivers:` section

```yaml
receivers:
  - name: 'my-serverless-signal'
    webhook_configs:
      - url: 'https://<LAMBDA_ENDPOINT>/alert?secret=<SERVER_SECRET>'
```

### Delete a number

To delete a number, and its stored state (session keys, etc):

```bash
curl -s  -XPOST -d '{"number": "+15555555555"}' https://<LAMBDA_ENDPOINT>/delete?secret=<SERVER_SECRET>
```

## HTTP API Reference

### Authentication

Authentication is handled via a single shared secret.

All endpoints require a query parameter `secret` that must equal the shared
secret configured on the server via `SIGNAL_SHARED_SECRET`.

* If this secret is missing or blank on the server, the server will return a `401`.
* If this secret is missing from the client's request, the server will return a `401`.

### Number IDs

In all lifecycle endpoints (register, verify, delete), the Signal account
number (used to send messages) is passed as the fully qualified number (e.g.,
`+15555555555`).

However in the endpoints that send messages, i.e., `/alert`, the raw number  is
not used. Instead a `sender` query parameter is required.  The value of
`sender` is the sha256 hash of the fully qualified number. This hash is
returned as part of the response from `/verify` and `/register`, so if you note
that down, there is no need to calculate it yourself.

This is done in order to prevent third-party services (alertmanager, datadog,
etc) from knowing what your Signal number is.

### POST /register

**JSON Payload:**

* `number`: string containing the fully qualified number, with no spaces or punctuation
* `verificationType`: a string containing one of `SMS` or `VOICE`

**Example Request:**
```json
{
    "number": "+15555555555",
    "verificationType": "SMS"
}
```

**Response Code:**

* `200`: if the registration request succeeded
* `400`: if the request payload is invalid
* `500`: if the  registration request failed

**Response Body:**

* `result`: a result message
* `numberId`: the id of the number registered (the sha256 hash), to be used when sending messages

**Example Response:**

```json
{
    "result": "registration requested with SMS for number '+15555555555'. respond with verification code.",
    "numberId": "910a625c4ba147b544e6bd2f267e130ae14c591b6ba9c25cb8573322dedbebd0",
}
```

### POST /verify

**JSON Payload:**

* `number`: string containing the fully qualified number, with no spaces or punctuation
* `code`: string containing the verification code, with no spaces or punctuation

**Example Request:**

```json
{
    "number": "+15555555555",
    "code": "123456"
}
```

**Response Code:**

* `200`: if the verification succeeded
* `400`: if the request payload is invalid
* `500`: if the verification failed

**Response Body:**

* `result`: a result message
* `numberId`: the id of the number registered (the sha256 hash), to be used when sending messages

**Example Response:**

```json
{
    "result":"the number '+15555555555' is now verified and ready to use",
    "numberId": "910a625c4ba147b544e6bd2f267e130ae14c591b6ba9c25cb8573322dedbebd0"
}
```

### POST /delete

To delete a number from the backend (but not from signal's servers).

**JSON Payload:**

* `number`: string containing the fully qualified number, with no spaces or punctuation

**Example Request:**

```json
{
    "number": "+15555555555"
}
```

**Response Code:**

* `200`: if the deletion succeeded
* `400`: if the request payload is invalid
* `500`: if the deletion failed

**Response Body:**

* `result`: a result message

**Example Response:**

```json
{
    "result":"the number '+15555555555' is deleted"
}
```

### POST /alert

An endpoint that receives [alertmanager]() formatted json payloads containing alerts. The Signal recipients are decided based on the
`receiver` attribute of the payload. See

**JSON Payload:**

See [alertmanager's <webhook_config>](https://prometheus.io/docs/alerting/configuration/#webhook_config)


**Example Request:**

Refer to `test/fixtures.js` for a contrived example.

**Response Code:**

* `200`: if the send succeeded
* `400`: if the request payload is invalid, or if the `sender` query parameter is missing
* `404`: if the `sender` query parameter is invalid (refers to non-existent number in the backend)
* `500`: if the send failed

**Response Body:**

* `result`: a result message

**Example Response:**

```json
{
    "result": "ok"
}
```

# License

`sigarillo-serverless` is licensed under the [GNU Affero General Public License
(AGPL) v3+](https://www.gnu.org/licenses/agpl-3.0.en.html).

Copyright (C) 2019  Abel Luck <abel@guardianproject.info> of [Guardian Project](https://guardianproject.info)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

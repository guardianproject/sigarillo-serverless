import * as chai from 'chai'
import sinon from 'sinon'
import {
  describe, it, beforeEach, afterEach,
} from 'mocha'
import * as config from '../src/config'
import alert from '../src/alert'

const { expect } = chai

describe('alert testing inputs', () => {
  let sandbox
  beforeEach((done) => {
    sandbox = sinon.createSandbox()
    sandbox.stub(config, 'default').value({
      SIGNAL_SHARED_SECRET: 'hello',
      SIGNAL_ALERTMANAGER_RECIPIENTS: 'receiver1=+111,receiver2=+222',
    })
    done()
  })

  it('errors on missing serverside secret, with undefined', async () => {
    sandbox.stub(config, 'default').value({ SIGNAL_SHARED_SECRET: undefined })
    const response = await alert({}, {})
    expect(response.statusCode).to.eq(401)
    expect(response.body).to.include('administrator')
  })

  it('errors on missing serverside secret, with empty string', async () => {
    sandbox.stub(config, 'default').value({ SIGNAL_SHARED_SECRET: '' })
    const response = await alert({}, {})
    expect(response.statusCode).to.eq(401)
    expect(response.body).to.include('administrator')
  })

  it('errors when secret doesnt match serverside', async () => {
    sandbox.stub(config, 'default').value({ SIGNAL_SHARED_SECRET: 'nope' })
    const queryStringParameters = { secret: 'hello' }
    const event = { queryStringParameters }
    const context = {}
    const response = await alert(event, context)
    expect(response.statusCode).to.eq(401)
    expect(response.body).to.include('not authorized')
  })

  it('errors when invalid json is passed', async () => {
    const queryStringParameters = { secret: 'hello', sender: 'XXX' }
    const event = { queryStringParameters, body: 'totally not json' }
    const context = {}
    const response = await alert(event, context)
    expect(response.statusCode).to.eq(400)
    expect(response.body).to.include('failed')
  })

  it('errors when no recipient is found', async () => {
    sandbox.stub(config, 'default').value({
      SIGNAL_SHARED_SECRET: 'hello',
      SIGNAL_ALERTMANAGER_RECIPIENTS: 'receiver404=+111',
    })
    const queryStringParameters = { secret: 'hello', sender: 'XXX' }
    const event = { queryStringParameters, body: JSON.stringify({ alerts: [] }) }
    const context = {}
    const response = await alert(event, context)
    expect(response.statusCode).to.eq(404)
    expect(response.body).to.include('recipient')
  })

  it('returns ok when no alerts are provided in the payload', async () => {
    const queryStringParameters = { secret: 'hello', sender: 'XXX' }
    const event = { queryStringParameters, body: JSON.stringify({ alerts: [], receiver: 'receiver1' }) }
    const context = {}
    const response = await alert(event, context)
    expect(response.statusCode).to.eq(200)
    expect(response.body).to.include('no alerts in payload')
  })

  afterEach((done) => {
    sandbox.restore()
    done()
  })
})

/* TODO test sending
describe('alert', function () {
  this.timeout(30000)
  context('testing sending', function () {
    let sandbox
    beforeEach(function (done) {
      sandbox = sinon.createSandbox()
      sandbox.stub(process, 'env').value(
        R.mergeRight(process.env, {
          'SIGNAL_SHARED_SECRET': 'hello',
        }))
      done()
    })

    it('sending', async function () {
      let queryStringParameters = { secret: 'hello' }
      let event = { queryStringParameters, body: JSON.stringify(alertsPayload) }
      let context = {}
      const response = await alert(event, context)
      expect(response.statusCode).to.eq(200)
      expect(response.body).to.include('ok')
    })

    afterEach(function (done) {
      sandbox.restore()
      done()
    })
  })
})
*/

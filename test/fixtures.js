import AWS from 'aws-sdk'

export const alertsPayload = {
  receiver: 'receiver1',
  status: 'firing',
  alerts: [{
    status: 'firing',
    labels: {
      alertname: 'DiskSpace',
      device: '/dev/sda1',
      fstype: 'ext4',
      host: 'foo',
      instance: '1.2.3.4',
      job: 'node',
      mountpoint: '/',
    },
    annotations: {
      description: 'Filesystem low',
      summary: 'Instance 1.2.3.4 filesystem usage is dangerously high',
    },
    startsAt: '2018-12-05T23:38:43.700314857Z',
    endsAt: '0001-01-01T00:00:00Z',
    generatorURL: 'http://1.2.3.4/graph?1',
  }, {
    status: 'firing',
    labels: {
      alertname: 'DiskSpace',
      device: '/dev/sda1',
      fstype: 'ext4',
      host: 'foo',
      instance: '1.2.3.4',
      job: 'node',
      mountpoint: '/',
    },
    annotations: {
      description: 'Filesystem low',
      summary: 'Instance 1.2.3.4 filesystem usage is dangerously high',
    },
    startsAt: '2018-12-05T23:38:43.700314857Z',
    endsAt: '0001-01-01T00:00:00Z',
    generatorURL: 'http://1.2.3.4/graph?1',
  }],
  groupLabels: {},
  commonLabels: {
    job: 'node',
  },
  commonAnnotations: {},
  externalURL: 'http://1.2.3.4',
  version: '4',
  groupKey: '{}:{}',
}

export const SSM = {
  Parameters: [
    {
      Name: '/sigarillo-serverless/testing/SIGNAL_ALERTMANAGER_RECIPIENTS',
      Type: 'SecureString',
      Value: 'receiver1=+111',
      Version: 1,
      LastModifiedDate: new Date(),
      ARN: 'arn:aws:ssm:eu-central-1:xxxx:parameter/sigarillo-serverless/testing/SIGNAL_ALERTMANAGER_RECIPIENTS',
    },
    {
      Name: '/sigarillo-serverless/testing/SIGNAL_SHARED_SECRET',
      Type: 'SecureString',
      Value: 'testsecret',
      Version: 1,
      LastModifiedDate: new Date(),
      ARN: 'arn:aws:ssm:eu-central-1:xxxx:parameter/sigarillo-serverless/testing/SIGNAL_SHARED_SECRET',
    },
  ],
  InvalidParameters: [],
}

export const mockSSM = (sandbox) => {
  sandbox.stub(process, 'env').value({
    SSM_PREFIX: 'sigarillo-serverless/testing',
  })
  sandbox.stub(AWS, 'SSM')
    .returns({
      getParameters() {
        return {
          promise() {
            return new Promise(resolve => setTimeout(() => resolve(SSM), 1))
          },
        }
      },
    })
}

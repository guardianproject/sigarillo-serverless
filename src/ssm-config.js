import EventEmitter from 'events'
import AWS from 'aws-sdk'

const DEFAULT_EXPIRY = 3 * 60 * 1000 // default expiry is 3 mins

async function fetchParameters(ssm, keyPrefix, keys) {
  const req = {
    Names: keys.map(k => `/${keyPrefix}/${k}`),
    WithDecryption: true,
  }
  const resp = await ssm.getParameters(req).promise()
  console.log('got resp', resp)
  const { Parameters } = resp

  const values = {}
  for (const p of Parameters) {
    const name = p.Name.replace(`/${keyPrefix}/`, '')
    values[name] = p.Value
  }
  return values
}

function validateKeys(keys, params) {
  const missing = keys.filter(k => params[k] === undefined)
  if (missing.length > 0) {
    throw new Error(`Missing SSM Parameter Store keys: ${missing}`)
  }
}

// eslint-disable-next-line object-curly-newline
function loadConfigs({ keys, ssm = new AWS.SSM(), keyPrefix = '', expiryMs = DEFAULT_EXPIRY }) {
  let isRefreshing = false
  if (!keys || !Array.isArray(keys) || keys.length === 0) {
    throw new Error('you need to provide a non-empty array of config keys')
  }

  if (expiryMs <= 0) {
    throw new Error('you need to specify an expiry (ms) greater than 0, or leave it undefined')
  }

  // the below uses the captured closure to return an object with a gettable
  // property per config key that on invoke:
  //  * fetch the config values and cache them the first time
  //  * thereafter, use cached values until they expire
  //  * otherwise, try fetching from SSM parameter store again and cache them
  const cache = {
    expiration: new Date(0),
    items: {},
  }
  const eventEmitter = new EventEmitter()

  const refresh = async () => {
    if (isRefreshing) { return }

    isRefreshing = true
    console.log(`Refreshing SSM Parameter Store keys: ${keys}`)
    try {
      const params = await fetchParameters(ssm, keyPrefix, keys)
      validateKeys(keys, params)
      console.log(`Successfully refreshed SSM Parameter Store keys: ${keys}`)
      const now = new Date()
      cache.expiration = new Date(now.getTime() + expiryMs)
      cache.items = params
      eventEmitter.emit('refresh')
    } finally {
      isRefreshing = false
    }
  }

  const getValue = (key) => {
    const now = new Date()
    if (now >= cache.expiration) {
      refresh()
    }
    return cache.items[key]
  }

  const config = {
    refresh,
    readyPromise: undefined,
    onRefresh: listener => eventEmitter.addListener('refresh', listener),
    onRefreshError: listener => eventEmitter.addListener('refreshError', listener),
  }
  const defaultKeys = Object.keys(config)

  for (let key of keys) {
    key = key.replace(keyPrefix, '')

    Object.defineProperty(config, key, {
      get() { return getValue(key) },
      enumerable: true,
      configurable: false,
    })
  }

  // catch usage of undefined keys
  if (process.env.NODE_ENV !== 'production') {
    return new Proxy(config, {
      get(target, key) {
        if (keys.includes(key) === false && defaultKeys.includes(key) === false) {
          throw new Error(`Key '${key}' is not defined in config keys`)
        }
        return target[key]
      },
    })
  }
  return config
}

export { loadConfigs }

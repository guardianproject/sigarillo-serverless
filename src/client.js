import R from 'ramda'
import {
  hashNumber, getState, createNumber, updateState, markVerified, getNumber, removeNumber,
} from './dynamodb'
import { logify } from './logfmtstringify'
import SignalService from './signal/signal'

export async function isNumberRegistered(number) {
  const value = await getNumber(hashNumber(number))
  return !R.isNil(value)
}

export async function isNumberRegisteredAndVerified(number) {
  const value = await getNumber(hashNumber(number))
  return !R.isNil(value) && value.isVerified
}

export async function registerNumber(number, useSMS = true) {
  const connection = new SignalService(number, {})
  if (useSMS) {
    await connection.requestSMSVerification(number)
  } else {
    await connection.requestVoiceVerification(number)
  }
  await createNumber(number, connection.getStoreData())
  return hashNumber(number)
}

export async function verifyNumber(number, code) {
  const hash = hashNumber(number)
  const state = await getState(hash)
  console.log('state is', state)
  if (R.isNil(state)) {
    throw new Error(`state was nil when fetching for verify operation for number ${number}`)
  }
  const connection = new SignalService(number, state)
  await connection.verifyNumber(number, code)
  await updateState(hash, connection.getStoreData())
  await markVerified(hash)
  return hash
}

export async function deleteNumber(number) {
  await removeNumber(hashNumber(number))
  return true
}

export async function sendAlert(senderHash, recipient, alert) {
  const data = await getNumber(senderHash)
  const { content, number } = data
  console.log(`sending alert for ${number} from ${senderHash} to ${recipient}`)
  const connection = new SignalService(number, content)

  try {
    const result = await connection.send(recipient, alert)
    console.log(logify(result))

    await updateState(senderHash, connection.getStoreData())
    return true
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err)
    throw new Error('message sending failed. see logs for more (sensitive) information')
  }
}

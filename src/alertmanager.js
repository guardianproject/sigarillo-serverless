import R from 'ramda'
import config from './config'

/**
 * Parse the input list of alertmanager receivers to signal recipients
 */
export function getRecipientForReceiver(inputReceiver, recipientsMapping) {
  const mapping = recipientsMapping || config.SIGNAL_ALERTMANAGER_RECIPIENTS
  const result = mapping.split(',')
    .map(item => item.split('='))
    .map(([receiver, recipient]) => ({ receiver, recipient }))
    .filter(m => inputReceiver === m.receiver)[0]
  return result ? result.recipient : undefined
}

/**
 * Format a single alert into a message string.
 */
export function formatAlert(data) {
  const parts = []

  if (data.status === 'firing') {
    parts.push('FIRING:')
  } else if (data.status === 'resolved') {
    parts.push('RESOLVED:')
  } else {
    parts.push(`${data.status.toUpperCase()}:`)
  }

  if (data.labels.host !== undefined) {
    parts.push(data.labels.host)
  } else if (data.labels.instance !== undefined) {
    parts.push(data.labels.instance)
  }

  parts.push('\n', data.annotations.description)
  parts.push('\n', data.generatorURL)

  return parts.join(' ')
}

/**
 * Parse AlertManager data object into an Array of message strings.
 */
export function parseAlerts(data) {
  if (R.isNil(data) || R.isNil(data.alerts)) {
    return []
  }

  const alerts = []

  data.alerts.forEach((alert) => {
    alerts.push(formatAlert(alert))
  })
  return alerts
}

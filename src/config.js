import R from 'ramda'
import { loadConfigs } from './ssm-config'

const envKeys = [
  'SIGNAL_DYNAMODB_TABLE',
  'SSM_PREFIX',
  'NODE_ENV',
  'DEBUG',
]

const envConf = R.pickAll(envKeys, process.env)

if (R.isNil(process.env.SSM_PREFIX)) {
  console.warn('WARNING: cannot load config from SSM, process.env.SSM_PREFIX is not defined')
}

const ssmConf = loadConfigs({
  keys: [
    'SIGNAL_SHARED_SECRET',
    'SIGNAL_ALERTMANAGER_RECIPIENTS',
  ],
  keyPrefix: process.env.SSM_PREFIX,
})

ssmConf.readyPromise = new Promise(resolve => ssmConf.onRefresh(() => resolve()))
ssmConf.refresh()

const mergedConf = Object.assign(ssmConf, envConf)

export default mergedConf

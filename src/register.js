// eslint-disable-line import/no-extraneous-dependencies, import/no-unresolved
import R from 'ramda'
import config from './config'
import initDebug from './debug'
import { isNumberRegistered, registerNumber } from './client'
import {
  configLoader, isBlank, makeError, makeResult,
} from './http'

initDebug()

// eslint-disable-next-line no-unused-vars,prefer-arrow-callback
export default configLoader(async function register(event, context) {
  if (isBlank(config.SIGNAL_SHARED_SECRET)) {
    return makeError(401, 'administrator must provide a \'SIGNAL_SHARED_SECRET\' env variable')
  }

  const { secret } = event.queryStringParameters
  if (secret !== config.SIGNAL_SHARED_SECRET) {
    return makeError(401, 'not authorized')
  }

  let body
  try {
    body = JSON.parse(event.body)
  } catch (err) {
    console.error(err)
    return makeError(400, 'failed to parse request body as json')
  }

  if (R.isNil(body.number) || R.isNil(body.verificationType)) {
    return makeError(400, 'invalid request: \'number\' and \'verificationType\' are required')
  }

  const { number, verificationType } = body


  let useSMS
  if (verificationType === 'SMS') {
    useSMS = true
  } else if (verificationType === 'VOICE') {
    useSMS = false
  } else {
    return makeError(400, 'invalid request: verificationType must be one of \'VOICE\' or \'SMS\'')
  }

  const alreadyRegistered = await isNumberRegistered(number)
  if (alreadyRegistered) {
    return makeError(400, `the number '${number}' is already registered. to re-register, first delete the number`)
  }

  try {
    const numberHash = await registerNumber(number, useSMS)
    return makeResult(200, `registration requested with ${verificationType} for number '${number}'. respond with verification code.`, { numberId: numberHash })
  } catch (err) {
    console.log(`failed to request registration of number '${number}' with verificationType ${verificationType}`)
    console.error(err)
    return makeError(500, 'failed to request registration of number')
  }
})

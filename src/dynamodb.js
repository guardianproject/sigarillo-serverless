import AWS from 'aws-sdk'
import crypto from 'crypto'
import config from './config'

export function hashNumber(number) {
  return crypto.createHash('sha256')
    .update(number)
    .digest('hex')
}

export async function getNumber(numberHash) {
  const params = {
    TableName: config.SIGNAL_DYNAMODB_TABLE,
    Key: {
      id: numberHash,
    },
  }
  console.log('getNumber', params)
  const dynamoDb = new AWS.DynamoDB.DocumentClient()
  const result = await dynamoDb.get(params).promise()
  return result.Item
}

export async function removeNumber(numberHash) {
  const params = {
    TableName: config.SIGNAL_DYNAMODB_TABLE,
    Key: {
      id: numberHash,
    },
  }
  console.log('removeNumber', params)
  const dynamoDb = new AWS.DynamoDB.DocumentClient()
  return dynamoDb.delete(params).promise()
}

export async function getState(numberHash) {
  const data = await getNumber(numberHash)
  console.log('getState', numberHash, data)
  return data.content
}

export async function createNumber(number, state) {
  const params = {
    TableName: config.SIGNAL_DYNAMODB_TABLE,
    Item: {
      id: hashNumber(number),
      number,
      content: state,
      isVerified: false,
      createdAt: Date.now(),
    },
  }
  console.log('createNumber', params)
  const dynamoDb = new AWS.DynamoDB.DocumentClient()
  return dynamoDb.put(params).promise()
}


export async function markVerified(numberHash) {
  const params = {
    TableName: config.SIGNAL_DYNAMODB_TABLE,
    Key: {
      id: numberHash,
    },
    UpdateExpression: 'SET isVerified = :isVerified',
    ExpressionAttributeValues: {
      ':isVerified': true,
    },
    ReturnValues: 'ALL_NEW',
  }
  console.log('markVerified', params)
  const dynamoDb = new AWS.DynamoDB.DocumentClient()
  return dynamoDb.update(params).promise()
}

export async function updateState(numberHash, state) {
  const params = {
    TableName: config.SIGNAL_DYNAMODB_TABLE,
    Key: {
      id: numberHash,
    },
    UpdateExpression: 'SET content = :content',
    ExpressionAttributeValues: {
      ':content': state || null,
    },
    ReturnValues: 'ALL_NEW',
  }
  console.log('updateState', params)
  const dynamoDb = new AWS.DynamoDB.DocumentClient()
  return dynamoDb.update(params).promise()
}

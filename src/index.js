/* eslint-disable no-global-assign */
require = require('esm')(module)
module.exports.alert = require('./alert.js').default
module.exports.register = require('./register.js').default
module.exports.verify = require('./verify.js').default
module.exports.delete = require('./delete.js').default

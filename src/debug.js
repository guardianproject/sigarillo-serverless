export default () => {
  if (console.debug === undefined) {
    console.debug = (...args) => {
      if (process.env.DEBUG) {
        console.log.apply(this, args)
      }
    }
  }
}
